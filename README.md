## New and improved (?) meet

#### Installation 
* Clone the repository
* run `npm install`
* run `npm start`

Or if you want to test it live, it's available at https://newandimprovedmeet.herokuapp.com

#### About

My thought about the app and my 'design': 
I wanted to keep it simple, and focus on functionality. 
* You're able to sort after name and offices 
* You're able to filter all employees in a search field
* Responsive design
* Works in Edge, firefox and chrome

I also wanted to take the opportunity to learn more about deploying with gitlab.
I've never used Heroku before and my experience with CI/CD in general is very low, that's one of the reasons I chose CI/CD.
One of the other reasons is that I believe having an automated work flow makes maintenance so much easier, especially when used with a code quality tool as ESLint. 

I wanted to focus on maintainability and that's why I wanted to add ESLint in CI in the first place.
Of course, maintainability also becomes higher when implementing tests, unfortunately I made the decision that I didn't have enough time due to my current knowledge about testing. 

In this writing moment I've spent approximately 5h 30m on this task, I've clocked every session and paused when going on breaks. I wanted to prove to myself what I can do in the given time without any cheating. Even though I wanted to cross off more features off the list, this is the stuff I could do, with my current knowledge and experience. 
