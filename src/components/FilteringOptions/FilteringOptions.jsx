import React from 'react';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Row from 'react-bootstrap/Row';
import PropTypes from 'prop-types';
import { sortOrderTypes } from '../../pages/Home/Home';

const FilteringOptions = ({ setSortOrder, setFilter }) => (
  <Row className="pt-3 pb-3">
    <Col xs={12}>
      <Navbar bg="light" expand="lg">
        <Form inline>
          <FormControl onChange={(e) => setFilter(e.currentTarget.value)} type="text" placeholder="Search" className="mr-sm-2" />
        </Form>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="Sorting" id="basic-nav-dropdown">
              <NavDropdown.Item onClick={() => setSortOrder(sortOrderTypes.NAME_ASC)}>
                By name
              </NavDropdown.Item>
              <NavDropdown.Item onClick={() => setSortOrder(sortOrderTypes.OFFICE_ASC)}>
                By office
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </Col>
  </Row>
);

FilteringOptions.propTypes = {
  setSortOrder: PropTypes.func.isRequired,
  setFilter: PropTypes.func.isRequired,
};

export default FilteringOptions;
