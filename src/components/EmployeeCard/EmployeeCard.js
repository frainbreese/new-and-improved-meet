import FA from 'react-fontawesome';
import React from 'react';

const githubBaseUrl = 'https://github.com/';
const stackOverFlowBaseUrl = 'https://stackoverflow.com/users';
const linkedInBaseUrl = 'https://linkedin.com';
const twitterBaseUrl = 'https://twitter.com';

const linkStyles = {
  color: 'black',
  padding: '5px',
};

const getContactLinks = (github, stackOverFlow, linkedIn, twitter) => {
  const linksArray = [];
  if (github) {
    linksArray.push(<a style={linkStyles} aria-label="link-to-employee-github" href={`${githubBaseUrl}/${github}`}><FA name="fas fa-github" /></a>);
  }

  if (stackOverFlow) {
    linksArray.push(<a style={linkStyles} aria-label="link-to-employee-github" href={`${stackOverFlowBaseUrl}/${stackOverFlow}`}><FA name="fas fa-stack-overflow" /></a>);
  }

  if (linkedIn) {
    linksArray.push(<a style={linkStyles} aria-label="link-to-employee-github" href={`${linkedInBaseUrl}/${linkedIn}`}><FA name="fab fa-linkedin" /></a>);
  }

  if (twitter) {
    linksArray.push(<a style={linkStyles} aria-label="link-to-employee-github" href={`${twitterBaseUrl}/${twitter}`}><FA name="fas fa-twitter" /></a>);
  }

  return linksArray;
};

export default getContactLinks;
