import React from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import FA from 'react-fontawesome';
import getContactLinks from './EmployeeCard';


const EmployeeCard = ({ employee }) => (
  <Col xs={12} md={6} xl={3}>
    <Card className="m-1 shadow-lg">
      <Row>
        <Col xs={12}>
          <Card.Img src={employee.imagePortraitUrl} style={{ width: '50%', marginLeft: '20%' }} />
        </Col>
      </Row>
      <Card.Body>
        <Row>
          <Col xs={8}>
            <h6 className="h6-responsive">
              {employee.name}
            </h6>
          </Col>
          <Col xs={4} className="d-flex justify-content-end">
            {getContactLinks(
              employee.gitHub,
              employee.stackOverflow,
              employee.linkedIn,
              employee.twitter,
            ).map((link, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <React.Fragment key={i}>
                {link}
              </React.Fragment>
            ))}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <FA name="fas fa-building" />
            {' '}
            {employee.office}
          </Col>
        </Row>
      </Card.Body>
    </Card>
  </Col>
);

EmployeeCard.propTypes = {
  employee: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    office: PropTypes.string,
    phoneNumber: PropTypes.string,
    tagLine: PropTypes.string,
    mainText: PropTypes.string,
    gitHub: PropTypes.string,
    twitter: PropTypes.string,
    stackOverflow: PropTypes.string,
    linkedIn: PropTypes.string,
    imagePortraitUrl: PropTypes.string,
    imageBodyUrl: PropTypes.string,
  }),
};

EmployeeCard.defaultProps = {
  employee: PropTypes.shape({
    name: PropTypes.string,
    office: PropTypes.string,
    email: PropTypes.string,
    phoneNumber: PropTypes.string,
    tagLine: null,
    mainText: PropTypes.string,
    gitHub: null,
    twitter: null,
    stackOverflow: null,
    linkedIn: null,
    imagePortraitUrl: PropTypes.string,
    imageBodyUrl: PropTypes.string,
  }),
};

export default EmployeeCard;
