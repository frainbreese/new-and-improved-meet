const fetchEmployees = async () => {
  const fetchUrl = 'https://api.tretton37.com/ninjas';
  return fetch(fetchUrl).then(async (res) => {
    const data = await res.json();
    return { success: true, data };
  })
    .catch(() => ({ success: false }));
};

export default fetchEmployees;
