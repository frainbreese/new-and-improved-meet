const sortOrderTypes = {
  NAME_ASC: 'NAME_ASC',
  OFFICE_ASC: 'OFFICE_ASC',
};

const getSortedEmployees = (employees, sortOrder) => {
  const arr = [...employees];
  switch (sortOrder) {
    case sortOrderTypes.NAME_ASC:
      return arr.sort((a, b) => (a.name > b.name ? 1 : -1));
    case sortOrderTypes.OFFICE_ASC:
      return arr.sort((a, b) => (a.office > b.office ? 1 : -1));
    default:
      return arr.sort((a, b) => (a.name > b.name ? 1 : -1));
  }
};

const getEmployeeStorage = () => {
  try {
    return JSON.parse(localStorage.employees);
  } catch (err) {
    return [];
  }
};

const setEmployeeStorage = (empls) => {
  localStorage.employees = JSON.stringify(empls);
};

const getFilteredEmployees = (employees, filter) => employees
  .filter((employee) => employee.name.toLowerCase().includes(filter.toLowerCase()));

export {
  getSortedEmployees, sortOrderTypes, getEmployeeStorage, setEmployeeStorage, getFilteredEmployees,
};
