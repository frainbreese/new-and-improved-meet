import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import FilteringOptions from '../../components/FilteringOptions/FilteringOptions';
import fetchEmployees from '../../util/EmployeeService';
import EmployeeCard from '../../components/EmployeeCard';
import {
  getEmployeeStorage, getFilteredEmployees, getSortedEmployees, setEmployeeStorage, sortOrderTypes,
} from './Home';

const Home = () => {
  const [storedEmployees, setStoredEmployees] = useState(getEmployeeStorage());
  const [employees, setEmployees] = useState(getEmployeeStorage());
  const [sortOrder, setSortOrder] = useState(sortOrderTypes.NAME_ASC);
  const [filter, setFilter] = useState('');

  useEffect(() => {
    (async () => {
      const response = await fetchEmployees();

      if (response.success) {
        setEmployees(getSortedEmployees(response.data));
        setEmployeeStorage(getSortedEmployees(response.data));
        setStoredEmployees(getSortedEmployees);
      }
    })();
  }, []);

  useEffect(() => {
    setEmployees(getSortedEmployees(employees, sortOrder));
    // eslint-disable-next-line
  }, [sortOrder]);

  useEffect(() => {
    setEmployees(getFilteredEmployees(storedEmployees, filter));
    // eslint-disable-next-line
  }, [filter]);

  return (
    <>
      <Container className="w-75" fluid>
        <FilteringOptions setSortOrder={setSortOrder} setFilter={setFilter} />
        <Row>
          {employees.map((employee) => (
            <EmployeeCard key={employee.name} employee={employee} />
          ))}
        </Row>
      </Container>
    </>
  );
};

export default Home;
