import React from 'react';
import AppStyles from './AppStyles';
import Home from './pages/Home';

function App() {
  return (
    <div className="App" style={AppStyles()}>
      <Home />
    </div>
  );
}

export default App;
